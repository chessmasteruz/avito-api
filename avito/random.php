<?php

function number(){
	
	return rand(-999999999, 999999999);
}


function guid($len = 0){
	
	if($len > 0){
		$str = "";
		
		for($i=0; $i<$len; $i++)
			$str .= dechex(rand(0,15));
		return $str;
	}
	
	return '"{'.guid(8)."-".guid(4)."-".guid(4)."-".guid(4)."-".guid(12).'}"';
	
}

function stringr($length = 0){
	
	if($length == 0){
		$length = rand(2, 12);
	}
	
	$str = "";
	
	for($i=0; $i <$length; $i++){
		$str .= chr(rand(97, 122));
	}
	
	return '"'.$str.'"';
	
}

function numstring($length = 0){
	
	if($length == 0){
		$length = rand(2, 12);
	}
	
	$str = "";
	
	for($i=0; $i <$length; $i++){
		$r = rand(87, 122);
		if($r > 96)
			$str .= chr($r);
		else 
			$str .= ($r-87);
	}
	
	return '"'.$str.'"';
}

function setr($set){
	$arr = json_decode($set);
	
	if(json_last_error() !== JSON_ERROR_NONE || count($arr) == 0){
		print "error data";
		exit;
	}
	
	$ans = $arr[rand(0,count($arr)-1)];
	
	if(is_numeric($ans)) return $ans;
	
	return '"' . $ans . '"';
	
}

?>
