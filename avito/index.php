<?php

require_once("db.php");
require_once("random.php");

session_start();

if(isset($_GET['route'])){
	
$route = trim($_GET['route'], '/');
$routeArr = explode('/', $route);



if($_SERVER['REQUEST_METHOD'] == 'POST' && $route == "api/generate"){
	
	if(isset($_SESSION['id']) && $_SESSION['data'] == json_encode($_POST) ){
		print $_SESSION['id'];
		exit;
	}
	
	$type = "number";
	$length = 0;
	$set = "";
	
	if(isset($_POST['type'])) $type = $_POST['type'];
	if(isset($_POST['length'])) $length = $_POST['length'];
	if(isset($_POST['set_list'])) $set = $_POST['set_list'];
	
	$random;
	
	switch($type){
		
		case "number":
		$random = number();
		break;
		
		case "guid":
		$random = guid();
		break;
		
		case "string":
		$random = stringr($length);
		break;
		
		case "numstring":
		$random = numstring($length);
		break;
		
		case "set":
		$random = setr($set);
		break;
		
		default:
		print "bad type";
		exit;
	}
	

	$sql = $pdo->prepare("INSERT INTO `random` (`random`) VALUES (?) ");
	$sql->execute(array($random));
	
	$id = $pdo->lastInsertId();
	
	$_SESSION['id'] = $id;
	$_SESSION['data'] = json_encode($_POST);
	
	print $id;
	
	exit;
	
} 

if($_SERVER['REQUEST_METHOD'] == 'GET' && count($routeArr) == 3)
	if($routeArr[0] == "api" && $routeArr[1] == "retrieve" && is_numeric($routeArr[2])){
		
			$sql = $pdo->prepare("SELECT * FROM `random` WHERE id=?");
			$sql->execute(array($routeArr[2]));
			
			if($sql->rowCount() != 1){
				$res = array("status" => "bad_id");	
				
			}				
			else {
				$res = array("status" => "ok", "result" => $sql->fetch());
			
			if(is_numeric($res['result']['random'])) $res['result']['random'] = (int)$res['result']['random'];
			else $res['result']['random'] = trim($res['result']['random'], '"');
			}
			
			header('Content-Type: application/json');
			print json_encode($res);
			exit;
		
	} 
}

	print "Nothing";	


?>